package piece;

/**
 * a knight piece
 * @author ZiqiSong
 * @author Ethan Smithweiss
 */
public class Knight extends Piece {

    public Knight(int posX, int posY, Color color) {
        super(posX, posY, Type.KNIGHT, color);
    }

    @Override
    public boolean checkPosition(int targetX, int targetY, Piece existing) {
        return (Math.abs(targetX - posX) == 2 && Math.abs(targetY - posY) == 1) ||
                (Math.abs(targetX - posX) == 1 && Math.abs(targetY - posY) == 2);
    }

    @Override
    public boolean movable() {
        return (check(posX +2,posY+1)) || (check(posX+2,posY-1))
                    || (check(posX-2,posY+1) || (check(posX-2,posY-1))
                    || (check(posX+1,posY-2)) || (check(posX-1,posY-2))
                    || check(posX+1,posY+2)) || (check(posX-1,posY+2));
    }
}
