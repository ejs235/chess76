package piece;

import error.GameOver;
import error.IllegalInput;
import error.LevelUp;

/**
 * a king piece
 * @author ZiqiSong
 * @author Ethan Smithweiss
 */
public class King extends Piece {

    public King(int posX, int posY, Color color) {
        super(posX, posY, Type.KING, color);
    }

    /**
     * king can only walk at most 1 step
     * @param targetX
     * @param targetY
     * @param existing
     * @return
     */
    @Override
    public boolean checkPosition(int targetX, int targetY, Piece existing) {
        return Math.abs(targetX - posX) <= 1 && Math.abs(targetY - posY) <= 1;
    }

    @Override
    public boolean movable() {
        return (check(posX + 1, posY) && !board.controlled(posX + 1, posY, Color.BLACK))
                || (check(posX + 1, posY + 1) && !board.controlled(posX + 1, posY + 1, Color.BLACK))
                || (check(posX, posY + 1) && !board.controlled(posX, posY + 1, Color.BLACK))
                || (check(posX - 1, posY + 1) && !board.controlled(posX - 1, posY + 1, Color.BLACK))
                || (check(posX - 1, posY) && !board.controlled(posX - 1, posY, Color.BLACK))
                || (check(posX - 1, posY - 1) && !board.controlled(posX - 1, posY - 1, Color.BLACK))
                || (check(posX, posY - 1) && !board.controlled(posX, posY - 1, Color.BLACK))
                || (check(posX + 1, posY - 1) && !board.controlled(posX + 1, posY - 1, Color.BLACK));
    }

    /**
     * check castling first when move king
     * @param toX
     * @param toY
     * @throws GameOver
     * @throws LevelUp
     * @throws IllegalInput
     */
    @Override
    public void moveTo(int toX, int toY) throws GameOver, LevelUp, IllegalInput {
        if (!castling(toX)) {
            if (check(toX, toY)) {
                leave();
                posX = toX;
                posY = toY;
                arrive();
            } else {
                throw new IllegalInput();
            }
        }
    }

    /**
     * if king is killed, game is over
     * @throws GameOver
     */
    @Override
    public void kill() throws GameOver {
        throw new GameOver(color == Color.WHITE ? Color.BLACK : Color.WHITE);
    }

    /**
     * check if castling is possible
     * move rook and king, and need not hasMoved
     * @param toX
     * @return
     * @throws LevelUp
     * @throws GameOver
     */
    private boolean castling(int toX) throws LevelUp, GameOver {
        if (hasMoved) {
            return false;
        }

        Color checkColor = color == Color.WHITE ? Color.BLACK : Color.WHITE;
        int rookX = toX > posX ? toX + 1 : toX - 1;
        Piece rook = board.getPiece(rookX, posY);
        if (rook == null || !(rook instanceof Rook) || rook.hasMoved) {
            return false;
        }

        if (board.getPiece(toX, posY) != null) {
            return false;
        }

        int rookAfter;
        if (rookX > posX) {
            rookAfter = rookX - 2;
            for (int i = posX; i < rookX; i++) {
                if (board.controlled(i, 0, checkColor)) {
                    return false;
                }
            }
        } else {
            rookAfter = rookX + 2;
            for (int i = rookX; i <= posX; i++) {
                if (board.controlled(i, 0, checkColor)) {
                    return false;
                }
            }
        }

        leave();
        posX = toX;
        arrive();
        rook.leave();
        rook.posX = rookAfter;
        rook.arrive();
        return true;
    }

    /**
     * check if the king is in checkmate status
     * @return
     */
    public boolean checkMate() {
        if (hasEnemy(posX, posY)) {
            return (!hasEnemy(posX + 1, posY) && check(posX + 1, posY))
                    || (!hasEnemy(posX - 1, posY) && check(posX - 1, posY))
                    || (!hasEnemy(posX, posY + 1) && check(posX, posY + 1))
                    || (!hasEnemy(posX, posY - 1) && check(posX, posY - 1))
                    || (!hasEnemy(posX + 1, posY + 1) && check(posX + 1, posY + 1))
                    || (!hasEnemy(posX - 1, posY + 1) && check(posX - 1, posY + 1))
                    || (!hasEnemy(posX - 1, posY - 1) && check(posX - 1, posY - 1))
                    || (!hasEnemy(posX + 1, posY - 1) && check(posX + 1, posY - 1));
        }

        return true;
    }

    /**
     * check horizontal threat
     * @return
     */
    private boolean hasThreatHor() {
        for (int i = posX + 2; i < 8; i++) {
            Piece tmp = board.getPiece(i, posY);
            if (tmp != null) {
                if (tmp.getColor() == color) {
                    break;
                }
                if (tmp.getColor() != color && (tmp instanceof Rook || tmp instanceof Queen)) {
                    return true;
                }
            }
        }

        for (int i = posX - 2; i >= 0; i--) {
            Piece tmp = board.getPiece(i, posY);
            if (tmp != null) {
                if (tmp.getColor() == color) {
                    break;
                }

                if (tmp.getColor() != color && (tmp instanceof Rook || tmp instanceof Queen)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * check vertical threat
     * @return
     */
    private boolean hasThreatVer() {
        for (int i = posY + 2; i < 8; i++) {
            Piece tmp = board.getPiece(posX, i);
            if (tmp != null) {
                if (tmp.getColor() == color) {
                    break;
                }
                if (tmp.getColor() != color && (tmp instanceof Rook || tmp instanceof Queen)) {
                    return true;
                }
            }
        }

        for (int i = posX - 2; i >= 0; i--) {
            Piece tmp = board.getPiece(posX, i);
            if (tmp != null) {
                if (tmp.getColor() == color) {
                    break;
                }

                if (tmp.getColor() != color && (tmp instanceof Rook || tmp instanceof Queen)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * check all enemies
     * @param x
     * @param y
     * @return
     */
    private boolean hasEnemy(int x, int y) {
        if (x > 7 || x < 0 || y > 7 || y < 0) {
            return true;
        }

        return hasEnemyPawn(x, y) || hasEnemyKnight(x, y) || hasEnemyBishop(x, y) || hasEnemyRookOrQueen();
    }

    /**
     * check rook and queen
     * @return
     */
    private boolean hasEnemyRookOrQueen() {
        return hasThreatVer() || hasThreatHor();
    }

    /**
     * check pawn
     * @param x
     * @param y
     * @return
     */
    private boolean hasEnemyPawn(int x, int y) {
        if (color == Color.WHITE) {
            Piece tmp = board.getPiece(x - 1, y + 1);
            if (tmp != null && tmp.getColor() != color && tmp instanceof Pawn) {
                return true;
            }
            tmp = board.getPiece(x + 1, y + 1);
            if (tmp != null && tmp.getColor() != color && tmp instanceof Pawn) {
                return true;
            }
        } else {
            Piece tmp = board.getPiece(x - 1, y - 1);
            if (tmp != null && tmp.getColor() != color && tmp instanceof Pawn) {
                return true;
            }
            tmp = board.getPiece(x + 1, y - 1);
            if (tmp != null && tmp.getColor() != color && tmp instanceof Pawn) {
                return true;
            }
        }

        return false;
    }

    /**
     * check bishop
     * @param x
     * @param y
     * @return
     */
    private boolean hasEnemyBishop(int x, int y) {
        int tmpX = x + 1;
        int tmpY = y + 1;
        while (tmpX < 8 && tmpX >= 0 && tmpY < 8 && tmpY >= 0) {
            Piece piece = board.getPiece(tmpX, tmpY);
            if (piece != null) {
                if (piece.getColor() != color && piece instanceof Bishop) {
                    return true;
                } else {
                    break;
                }
            }
            tmpX++;
            tmpY++;
        }

        while (tmpX < 8 && tmpX >= 0 && tmpY < 8 && tmpY >= 0) {
            Piece piece = board.getPiece(tmpX, tmpY);
            if (piece != null) {
                if (piece.getColor() != color && piece instanceof Bishop) {
                    return true;
                } else {
                    break;
                }
            }
            tmpX--;
            tmpY++;
        }

        while (tmpX < 8 && tmpX >= 0 && tmpY < 8 && tmpY >= 0) {
            Piece piece = board.getPiece(tmpX, tmpY);
            if (piece != null) {
                if (piece.getColor() != color && piece instanceof Bishop) {
                    return true;
                } else {
                    break;
                }
            }
            tmpX++;
            tmpY--;
        }

        while (tmpX < 8 && tmpX >= 0 && tmpY < 8 && tmpY >= 0) {
            Piece piece = board.getPiece(tmpX, tmpY);
            if (piece != null) {
                if (piece.getColor() != color && piece instanceof Bishop) {
                    return true;
                } else {
                    break;
                }
            }
            tmpX--;
            tmpY--;
        }

        return false;
    }

    /**
     * check knight
     * @param x
     * @param y
     * @return
     */
    private boolean hasEnemyKnight(int x, int y) {
        Piece tmp = board.getPiece(x - 2, y + 1);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }
        tmp = board.getPiece(x + 2, y - 1);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }
        tmp = board.getPiece(x + 2, y + 1);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }
        tmp = board.getPiece(x - 2, y - 1);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }

        tmp = board.getPiece(x - 1, y - 2);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }
        tmp = board.getPiece(x + 1, y - 2);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }
        tmp = board.getPiece(x + 1, y + 2);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }
        tmp = board.getPiece(x - 1, y - 2);
        if (tmp != null && tmp.getColor() != color && tmp instanceof Knight) {
            return true;
        }
        return false;
    }
}
