package error;

import piece.Piece.Color;

/**
 * game over error
 * @author ZiqiSong
 * @author Ethan Smithweiss
 */
public class GameOver extends Exception{
    // win color
    Color winColor;

    /**
     * constructor
     * @param winColor
     */
    public GameOver(Color winColor) {
        this.winColor = winColor;
    }

    /**
     * get color
     * @return
     */
    public Color getWinColor() {
        return winColor;
    }
}
