package board;

import error.GameOver;
import error.IllegalInput;
import error.LevelUp;
import error.NoMove;
import java.nio.file.WatchEvent.Kind;
import java.util.Scanner;
import piece.Piece.Color;
import java.util.ArrayList;
import java.util.List;
import piece.Bishop;
import piece.King;
import piece.Knight;
import piece.Pawn;
import piece.Piece;
import piece.Queen;
import piece.Rook;

/**
 * chessboard class
 * it's a single instance
 * @author ZiqiSong
 * @author Ethan Smithweiss
 */
public class Board {

    // single instance
    private static Board INSTANCE = new Board();

    /**
     * get instance
     * @return
     */
    public static Board getBoard() {
        return INSTANCE;
    }

    // white pieces
    private List<Piece> whites;
    // black pieces
    private List<Piece> blacks;
    // board
    private Piece[][] chessboard;

    /**
     * constructor
     */
    private Board() {
        chessboard = new Piece[8][8];
        whites = new ArrayList<>(24);
        blacks = new ArrayList<>(24);
        init();
    }

    /**
     * init chessboard, add white and black pieces
     */
    private void init() {
        for (int i = 0; i < 8; i++) {
            chessboard[i] = new Piece[8];
        }

        addPiece(new King(4, 0, Color.WHITE));
        addPiece(new Queen(3, 0, Color.WHITE));
        addPiece(new Rook(0, 0, Color.WHITE));
        addPiece(new Rook(7, 0, Color.WHITE));
        addPiece(new Knight(1, 0, Color.WHITE));
        addPiece(new Knight(6, 0, Color.WHITE));
        addPiece(new Bishop(5, 0, Color.WHITE));
        addPiece(new Bishop(2, 0, Color.WHITE));

        addPiece(new King(4, 7, Color.BLACK));
        addPiece(new Queen(3, 7, Color.BLACK));
        addPiece(new Rook(0, 7, Color.BLACK));
        addPiece(new Rook(7, 7, Color.BLACK));
        addPiece(new Knight(1, 7, Color.BLACK));
        addPiece(new Knight(6, 7, Color.BLACK));
        addPiece(new Bishop(5, 7, Color.BLACK));
        addPiece(new Bishop(2, 7, Color.BLACK));

        for (int i = 0; i < 8; i++) {
            addPiece(new Pawn(i, 1, Color.WHITE));
            addPiece(new Pawn(i, 6, Color.BLACK));
        }
    }

    /**
     * add one piece, add to list and put it on chessboard
     * @param piece
     */
    private void addPiece(Piece piece) {
        if (piece.getColor() == Color.WHITE) {
            whites.add(piece);
        } else {
            blacks.add(piece);
        }

        chessboard[piece.getPosX()][piece.getPosY()] = piece;
    }

    /**
     * put one piece on chessboard
     * @param piece
     */
    public void setPiece(Piece piece) {
        chessboard[piece.getPosX()][piece.getPosY()] = piece;
    }

    /**
     * clear a position
     * @param x
     * @param y
     */
    public void clearPiece(int x, int y) {
        chessboard[x][y] = null;
    }

    /**
     * check if current color is movable
     * @param color
     * @return
     */
    public boolean movable(Color color) {
        if (color == Color.WHITE) {
            for (Piece piece : whites) {
                if (piece != null && !piece.isDead() && piece.movable()) {
                    return true;
                }
            }

            return false;
        } else {
            for (Piece piece : blacks) {
                if (piece != null && !piece.isDead() && piece.movable()) {
                    return true;
                }
            }

            return false;
        }
    }

    /**
     * check if current position is controlled
     * @param x
     * @param y
     * @param color
     * @return
     */
    public boolean controlled(int x, int y, Color color) {
        if (color == Color.WHITE) {
            return controlled(x, y, whites);
        } else {
            return controlled(x, y, blacks);
        }
    }

    /**
     * check controlled
     * @param x
     * @param y
     * @param pieces
     * @return
     */
    private boolean controlled(int x, int y, List<Piece> pieces) {
        for (int i = 0; i < pieces.size(); i++) {
            if (pieces.get(i) == null || !pieces.get(i).isDead()) {
                continue;
            }
            if (pieces.get(i).control(x, y)) {
                return true;
            }
        }
        return false;
    }

    /**
     * check if the king is checkmate
     * @param color
     * @return it means a king is checkmate if returns a king
     */
    private King checkKing(Color color) {
        for (Piece[] pieces: chessboard) {
            if (pieces == null) {
                continue;
            }
            for (Piece piece: pieces) {
                if (piece != null && piece.getColor() != color && piece instanceof King) {
                    King king = (King) piece;
                    if (!king.checkMate()) {
                        return king;
                    }
                }
            }
        }

        return null;
    }

    /**
     * move a piece
     * @param fromX
     * @param fromY
     * @param toX
     * @param toY
     * @param color
     * @param lUpPiece
     * @throws IllegalInput
     * @throws GameOver
     * @throws NoMove
     */
    public void move(int fromX, int fromY, int toX, int toY, Color color,String lUpPiece) throws IllegalInput, GameOver, NoMove {
        Piece piece = getPiece(fromX, fromY);
        try {
            piece.moveTo(toX, toY);
        } catch (LevelUp e) {
            // if a pawn reach the end, upgrade it
            int x = piece.getPosX();
            int y = piece.getPosY();

            Piece upPiece;
            
                switch (lUpPiece) {
                    case "Q":
                        upPiece = new Queen(x, y, piece.getColor());
                        break;
                    case "R":
                        upPiece = new Rook(x, y, piece.getColor());
                        break;
                    case "N":
                        upPiece = new Knight(x, y, piece.getColor());
                        break;
                    case "B":
                        upPiece = new Bishop(x, y, piece.getColor());
                        break;
                    default:
                    	//System.out.printf("you shouldn't be here\n");
                        upPiece = new Queen(x, y, piece.getColor());
                }
            
            setPiece(upPiece);
            if (piece.getColor() == Color.WHITE) {
                whites.add(upPiece);
            } else {
                blacks.add(upPiece);
            }

            piece.setPos(999, 999);
        } finally {
            // print and check king
        	System.out.println();
            print();
            System.out.println();
            King king = checkKing(color);
            if (king != null) {
                System.out.println("Checkmate");
                Color tmp = king.getColor() == Color.WHITE ? Color.BLACK : Color.WHITE;
                System.out.println(tmp + " wins");
                System.exit(0);
            }
        }

        Color nextColor = color == Color.WHITE ? Color.BLACK : Color.WHITE;
        if (!movable(nextColor)) {
            throw new NoMove();
        }
    }

    /**
     * get piece on board
     * @param x
     * @param y
     * @return
     */
    public Piece getPiece(int x, int y) {
        if (x < 0 || x > 7 || y < 0 || y > 7) {
            return null;
        }
        return chessboard[x][y];
    }

    /**
     * get piece by color and index
     * @param index
     * @param color
     * @return
     */
    public Piece getPiece(int index, Color color) {
        if (color == Color.WHITE) {
            return whites.get(index);
        } else {
            return blacks.get(index);
        }
    }

    /**
     * check if path from src to target is empty
     * @param fromX
     * @param fromY
     * @param toX
     * @param toY
     * @return
     */
    public boolean pathEmpty(int fromX, int fromY, int toX, int toY) {
        int minX = Math.min(fromX, toX);
        int maxX = Math.max(fromX, toX);
        int minY = Math.min(fromY, toY);
        int maxY = Math.max(fromY, toY);
        if (minX == maxX) {
            for (int y = minY + 1; y < maxY; y++) {
                if (chessboard[maxX][y] != null) {
                    return false;
                }
            }
        } else if (minY == maxY) {
            for (int x = minX + 1; x < maxX; x++) {
                if (chessboard[x][maxY] != null) {
                    return false;
                }
            }
        } else if (Math.abs(maxY - minY) == Math.abs(maxX - minX)) {
            int directionX = fromX > toX ? -1 : 1;
            int directionY = fromY > toY ? -1 : 1;
            int x = fromX + directionX;
            int y = fromY + directionY;
            while (x != toX && y != toY) {
                if (chessboard[x][y] != null) {
                    return false;
                }

                x += directionX;
                y += directionY;
            }

            return true;
        } else {
            return false;
        }

        return true;
    }

    /**
     * print chessboard
     */
    public void print() {
        int row, col;
        for (row = 8; row > 0; row--) {
            for (col = 1; col <= 8; col++) {
                if (chessboard[col - 1][row - 1] != null) {
                    System.out.print(" " + chessboard[col - 1][row - 1].getSymbol());
                } else {
                    String symbol = (row + col) % 2 == 1 ? "  " : "##";;
                    System.out.print(" " + symbol);
                }
            }
            System.out.print(" " + row + "  ");
            System.out.println();
        }
        int a = 'a';
        System.out.print(" ");
        for (col = 0; col < 8; col++) {
            System.out.print(" " + (char) (a + col) + " ");
        }
        System.out.println();
        System.out.println();
    }

}
